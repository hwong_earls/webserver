'use strict';

import React from 'react';
import { Chart_Component } from './chart_component.js';
import { Map } from './map_component.js';
import { No_City_Component } from './no_city_component.js';
import { QUEUE, parseDate } from '../commons/library.js';


export class Content_Current extends React.Component
{
    constructor(props) {
        super(props);
        this.state = { 'city': null };
        const $this = this;            
        QUEUE.addSubscribe({ 'queue': 'filter_current', 'subscriber': function(data) {
            $this.changeData($this, data);
        } });        
    }

    changeData($this, data) {
        $this.setState({ 'city': data });
        googleMapChangeLocation(data.coord);
    }

    render() {

        if(this.state.city == null){

            return (
                    <No_City_Component />
            );
        }
                
        return (
                <div className='row'>
            		<div className='col-sm-6 current_section'>
                		<div className='row'>
                			<div className='col-sm-12'><span className='title_current'>Current Condition:</span></div>
                		</div>
                		<div className='row'>
                			<div className='col-sm-4'>City:</div>
                			<div className='col-sm-8'>{this.state.city.name}, {this.state.city.sys.country}</div>
                		</div>
                		<div className='row'>
                			<div className='col-sm-4'>Condition:</div>
                			<div className='col-sm-8'>{this.state.city.weather[0].main}</div>
                		</div>
                		<div className='row'>
                			<div className='col-sm-4'>Description:</div>
                			<div className='col-sm-8'>{this.state.city.weather[0].description}</div>
                		</div>
                		<div className='row'>
                			<div className='col-sm-4'>Temperature:</div>
                			<div className='col-sm-8'>{this.state.city.main.temp}&nbsp;&deg;C</div>
                		</div>
                		<div className='row'>
                			<div className='col-sm-4'>Min. Temperature:</div>
                			<div className='col-sm-8'>{this.state.city.main.temp_min}&nbsp;&deg;C</div>
                		</div>
                		<div className='row'>
                			<div className='col-sm-4'>Max. Temperature:</div>
                			<div className='col-sm-8'>{this.state.city.main.temp_max}&nbsp;&deg;C</div>
                		</div>
                		<div className='row'>
                			<div className='col-sm-4'>Humidity:</div>
                			<div className='col-sm-8'>{this.state.city.main.humidity}&nbsp;%</div>
                		</div>
                		<div className='row'>
                			<div className='col-sm-4'>Wind Speed:</div>
                			<div className='col-sm-8'>{this.state.city.wind.speed}&nbsp;Km/H</div>
                		</div>
            	</div>
            	<div className='col-sm-6' id='map-container'>
                	<Map />
           	    </div>
			</div>
        );
    }
}


export class Content_Forecast extends React.Component
{
    constructor(props) {
        super(props);
        this.state = { 'forecast': null };
        const $this = this;
        QUEUE.addSubscribe({ 'queue': 'filter_forecast', 'subscriber': function(data) {
            $this.changeData($this, data);
        } });
    }

    changeData($this, data) {
        $this.setState({ 'forecast': data });
    }

    render() {
        
        if(this.state.forecast == null){

            return (
            	<div className='container'>
                </div>
            );
        }

        var forecast_days = [];
        var countDays = 1;
        forecast_days.push(
            <div className='col-sm-3 col-sm-offset-1' key='forecast-headers'>
                <div className='row'>
                    <div className='col-sm-12 forecast-tab-even'><span className='forecast-tab-labels'>Day:</span></div>
                </div>
                <div className='row'>
                    <div className='col-sm-12 forecast-tab-even'><span className='forecast-tab-labels'>&nbsp;</span></div>
                </div>
                <div className='row'>
                    <div className='col-sm-12 forecast-tab-odd'><span className='forecast-tab-labels'>Condition:</span></div>
                </div>
                <div className='row'>
                    <div className='col-sm-12 forecast-tab-even'><span className='forecast-tab-labels'>Temperature:</span></div>
                </div>
                <div className='row'>
                    <div className='col-sm-12 forecast-tab-odd'><span className='forecast-tab-labels'>Min. Temp.:</span></div>
                </div>
                <div className='row'>
                    <div className='col-sm-12 forecast-tab-even'><span className='forecast-tab-labels'>Max. Temp.:</span></div>
                </div>
            </div>
        );

        var forecastData = {
            temperature: [],
            max: [],
            min: []
        };        
        for(var days of this.state.forecast.list){
            var key = 'forecast-' + countDays;
            var date = parseDate(days.dt);
            forecast_days.push(
                <div className='col-sm-1' key={key}>
                    <div className='row'>
                    	<div className='col-sm-12 forecast-tab-even'><span className='forecast-tab-data'>{date.weekday}</span></div>
                    </div>
                    <div className='row'>
                    	<div className='col-sm-12 forecast-tab-even'><span className='forecast-tab-data'>{date.dateFormatted}</span></div>
                    </div>
                    <div className='row'>
                    	<div className='col-sm-12 forecast-tab-odd'><span className='forecast-tab-data'>{days.weather[0].main}</span></div>
                    </div>
                    <div className='row'>
                    	<div className='col-sm-12 forecast-tab-even'><span className='forecast-tab-data'>{days.temp.day}&nbsp;&deg;C</span></div>
                    </div>
                    <div className='row'>
                    	<div className='col-sm-12 forecast-tab-odd'><span className='forecast-tab-data'>{days.temp.min}&nbsp;&deg;C</span></div>
                    </div>
                    <div className='row'>
                    	<div className='col-sm-12 forecast-tab-even'><span className='forecast-tab-data'>{days.temp.max}&nbsp;&deg;C</span></div>
                    </div>
                </div>
            );
            countDays++;
            forecastData.temperature.push(parseFloat(days.temp.day));
            forecastData.max.push(parseFloat(days.temp.max));
            forecastData.min.push(parseFloat(days.temp.min));
        }


        return (
            <div className='col-sm-12 forecast_section'>
                <div className='row'>
                	<Chart_Component data={forecastData} />
                </div>
                <div className='row forecast-tab'>
                	{forecast_days}
                </div>
            </div>
        );
    }
}



