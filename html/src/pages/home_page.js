'use strict';

import React from 'react';

import { Content_Current, Content_Forecast } from '../components/content_component.js';
import { Filter } from '../components/filter_component.js';


export class Home_Page extends React.Component
{
    render() {
        
        return (
            <div className='row'>
                <div className='col-sm-12'>
                	<Filter />
                	<Content_Current />
                	<div className='row'>
                		<Content_Forecast />
                    </div>
                </div>
            </div>
        );
    }
}
