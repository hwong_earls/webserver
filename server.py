import string

"""
	Add: lib to the path of libraries for Python
"""
import vendor
vendor.add('lib')


from flask import Flask, request, send_from_directory
from flask_cors import CORS, cross_origin


app = Flask(__name__)
CORS(app)


_PREFIX_PATH = 'html'
_LISTENING_PORT = 8080


"""
	Response function for deliver the statics pages
"""
def response(path):

    temporal = string.split(path, '/')
    filename = temporal.pop()
    pathfile = '/'.join(temporal)

    return send_from_directory('./{}/{}'.format(_PREFIX_PATH, \
                                                pathfile), filename)


@app.route('/api', methods = ['GET'])
def api():

    return 'Hello'


@app.route('/', defaults={'path':''})
@app.route('/<path:path>')
def index(path):

    if path == '':

        return send_from_directory('./{}'.format(_PREFIX_PATH), 'index.html')

    return response(path)


if __name__ == '__main__':

    app.run(host = '0.0.0.0', port = _LISTENING_PORT)
