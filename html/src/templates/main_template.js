'use strict';

import React from 'react';
import { Route } from 'react-router-dom';

import { Footer } from '../components/footer_component.js';
import { Header } from '../components/header_component.js';

import { Home_Page } from '../pages/home_page.js';
import { About_Page } from '../pages/about_page.js';
import { Contact_Page } from '../pages/contact_page.js';


export class Main_Template extends React.Component
{
    render() {
        
        return (
    		<div className='container'>
                <Header />
            	<div className='container'>
                		<Route exact path='/' component={ Home_Page } />
                 		<Route path='/about' component={ About_Page } />
                 		<Route path='/contact' component={ Contact_Page } />
            	</div>
                <Footer />
            </div>
        );
    }
}
