'use strict';

import React from 'react';
import Chart from 'chart.js';


export class Chart_Component extends React.Component
{
    constructor(props) {

        super(props);
    }

    setChart() {
        var $chartCanvas = document.getElementById('chart-canvas');
        var chartData = {
            type: 'line',
            data: {
                labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
                datasets: [
                    {
                        label: 'Temperature',
                        borderWidth: 1,
                        fill: false,
                        borderColor: '#83ed1a',
                        data: this.props.data.temperature
                    },
                    {
                        label: 'Max. Temp.',
                        borderWidth: 1,
                        fill: false,
                        borderColor: '#ba0b0b',
                        data: this.props.data.max
                    },
                    {
                        label: 'Min. Temp.',
                        borderWidth: 1,
                        fill: false,
                        borderColor: '#4046f7',
                        data: this.props.data.min
                    }
                ]
            },
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: 'Five Days Forecast Chart'
                },
                legend: {
                    display: true,
                    position: 'bottom'
                },
                scales: {
                    yAxes: [
                        {
                            display: true,
                            scaleLabel: {
                                labelString: 'Celsius',
                                display: true
                            }
                        }
                    ]
                }
            }
        };
        var chart = new Chart($chartCanvas, chartData);
    }

    componentDidMount() {
        this.setChart();        
    }

    componentDidUpdate() {
        this.setChart();
    }

    render() {
        
        return (
            <canvas id='chart-canvas' className='chart-style'></canvas>
        );
    }
}
