'use strict';

import React from 'react';


export class No_City_Component extends React.Component
{

    render() {

        return (
            <div className='row'>
                <p className='no-city-title'>There is not a city selected</p>
                <p>Please, type the name of city follow by the two characters county code and click on 'Get Weather' for check the forecast.</p>
                <p>Example:</p>
                <p className='no-city-example'>Vancouver,ca</p>
                <p className='no-city-example'>Beijing,cn</p>
            </div>
        );
    }
}
