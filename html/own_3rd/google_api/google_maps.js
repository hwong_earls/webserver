/*
 *	Global function for google maps
 */

var GLOBAL_MAP;

function googleMapChangeLocation(coord) {
    var centerCoord = new google.maps.LatLng(coord.lat, coord.lon);
    GLOBAL_MAP.setCenter(centerCoord);
    var marker = new google.maps.Marker({ position: centerCoord });
    marker.setMap(GLOBAL_MAP);
}

function googleMap() {

    var centerCoord = new google.maps.LatLng(49.241284, -123.113488);
    var properties = { center: centerCoord,  zoom: 10 };

    GLOBAL_MAP = new google.maps.Map(document.getElementById('map'), properties);
}
