'use strict';

import React from 'react';
import { Link } from 'react-router-dom';

export class Header extends React.Component
{
    render() {
        
        return (
            <div className='container'>
            	<div className='row header-section'>                	
                	<div className='header-blocks'>
                		<img src='images/weather.png' alt='GSL Weather Forecast' />
                	</div>
                	<div className='header-blocks header-block-title'>
                		<span className='header-title'>ACME Weather</span>
                	</div>
                </div>
                <div className='row'>
            		<nav className='navbar navbar-default'>
                		<ul className='nav navbar-nav'>
                			<li><Link to='/'>Home</Link></li>
                			<li><Link to='/about'>About the Project</Link></li>
                			<li><Link to='/contact'>Contact</Link></li>
                		</ul>
            		</nav>
                </div>
            </div>
        );        
    }
}
