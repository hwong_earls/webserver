'use strict';

import React from 'react';


export class Contact_Page extends React.Component
{
    render() {
        
        var email = 'henrywong72@hotmail.com';

        return (
            <div className='row'>
                <p className='contact-title'>Contact</p>
                <p className='contact-content'>
                	<b>Author:</b> Henry Wong
                </p>
                <p className='contact-content'>
                	<b>Email:</b> <a href='mailto:henrywong72@hotmail.com'>{email}</a>
                </p>
                <p className='contact-content'>
                	<b>Linkedin:</b> <a href='https://www.linkedin.com/in/henry-wong-a801738/'>Henry Wong</a>
                </p>
            </div>
        );
    }
}
