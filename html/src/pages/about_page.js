'use strict';

import React from 'react';


export class About_Page extends React.Component
{
    render() {
        
        return (
            <div className='container'>
            <p className='about-title'>About the project</p>
            <p className='about-content'>
            	This small web application is a demo of: Python and ReactJS. It is available at: <a href='https://hwong_earls@bitbucket.org/hwong_earls/webserver.git'>Bitbucket</a>.<br/>               
                The project is composed by two components: Back-End and Front-End where:<br/>
            </p>
            <p className='about-content'>
                <b>Back-End:</b><br/>
                It is a small Python with Flask web application used as webserver and for implement simply API.
            </p>
            <p className='about-content'>
                <b>Front-End:</b><br/>
                The front-end is implementing with: HTML, CSS, Javascript and ReactJS with help of third parties libraries such as:<br/>
                <ul>
                	<li>JQuery</li>
                	<li>BootstrapJS</li>
                	<li>ChartJS</li>
					<li>Google Maps API</li>
                </ul>
            </p>
            <p className='about-content'>
                <b>Content:</b><br/>
                <ul>
                	<li><b>Vagrantfile</b>, for create the environment in a virtual machine. Execute vagrant with: --provision option.</li>
                	<li><b>Source code</b>, Git repository with all the code.</li>
                </ul>
            </p>
            </div>
        );
    }
}
