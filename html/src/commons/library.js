"use strict";

export function parseDate(unixtimestamp) {

    var day = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    var month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var date = new Date(parseInt(unixtimestamp) * 1000);

    var dateFormatted = month[date.getMonth()] + ' ' + date.getDate();

    return {
        'weekday': day[date.getDay()],
        'dateFormatted':  dateFormatted
    };
}

/*
	callApi	:	Make asynchronous AJAX call

    Parameters:
    	method : [GET, POST], default: GET
        data   : Object with all the data to pass to the back-end in JSON format
        action : Url of the API method to call
        callback : Function call after sucesses call to the API passing Object with API response
*/
export function callApi(method = 'GET', data = {}, action = '', callback = null) {

    try {
        var xmlhttp = new XMLHttpRequest();
    
        xmlhttp.open(method, getUrl(action));

        xmlhttp.onreadystatechange = function() {
            if(xmlhttp.readyState == XMLHttpRequest.DONE && xmlhttp.status == 200) {            
                callback(JSON.parse(xmlhttp.response));
            }
        }

        if(method == 'POST'){
            xmlhttp.send(JSON.stringify(data));
        }
        else if(method == 'GET'){
            xmlhttp.send();
        }
    }
    catch(e){
        console.log(e);
    }
}

/*
	getUrl: Function for add the API key to the url request instead of add the key to all the API requests

    To Do:
    - No very useful for multi APIs, require to add a list of keys and select one based on the request
*/
function getUrl(url) {

    return url + '&APPID=44090464cab67d5895559eb4f749c47c';
}

class Observer 
{
    constructor() {
        this.subscribers = [];
    }

    addSubscribe(subscriber) {
        /*
          	Subscriber:
            	Subscriber: Function represent the subscriber itself
                Queue: String with the name of the queue, Queue is logical 
            Example:
				QUEUE.addSubscribe({ 'queue': '<name of the queue>', 'subscriber': function() { <statements> } });
         */
        var queue = this._getQueueName(subscriber['queue']);
        var subscriberFunction = subscriber['subscriber'];
        if(this.subscribers[queue] == null){
            this.subscribers[queue] = [];
        }
        this.subscribers[queue].push(subscriberFunction);

        return this.subscribers;
    }

    publish(message) {
        /*
          	Message:
				Queue: String with the name of the queue where to send the message
                Data: Map object with the message publisher
            Example:
				QUEUE.publish({ 'queue': '<name of the queue>', 'data': { <Something> } });
         */
        var queue = this._getQueueName(message['queue']);
        var data = message['data']

        if(this.subscribers[queue] == null){
            console.log('Error: Queue does not exists in observer');
        }
        for(var subscriberFunction of this.subscribers[queue]){
            subscriberFunction(data);
        }
    }

    _getQueueName(name) {
        /*
          	Use of function for compose the name of the queue in order to use names like: filter
            which: filter is a reservate name because it is a builtin function
         */
        return 'queue_' + name;
    }
}

export var QUEUE = new Observer();
