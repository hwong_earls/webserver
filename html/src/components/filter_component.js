'use strict';

import React from 'react';
import { callApi, QUEUE } from '../commons/library.js';


export class Filter extends React.Component
{
    constructor(props) {
        super(props);

        this.state = { 'city': '' };
        this.changeCity = this.changeCityAction.bind(this);
        this.searchAction = this.searchAction.bind(this);
    }

    searchAction(event) {

        var cityQuery = this.state.city;
        var apiUrl = 'http://api.openweathermap.org/data/2.5/weather?units=metric&q='+cityQuery;
        callApi('GET', null, apiUrl, function(data) {
            QUEUE.publish({ 'queue': 'filter_current', 'data': data });
        } );
        var apiUrl = 'http://api.openweathermap.org/data/2.5/forecast/daily?cnt=7&units=metric&q='+cityQuery;
        callApi('GET', null, apiUrl, function(data) {
            QUEUE.publish({ 'queue': 'filter_forecast', 'data': data });
        } );
        event.preventDefault();
    }

    changeCityAction(event) {
        this.setState({ 'city': event.target.value });
    }

    render() {
        
        var style = { 'width': '100%' };

        return (
        	<form className='form-inline filter_section' onSubmit={this.searchAction}>
                <div className='row'>
                <div className='col-sm-12'>
                	<span className='filter_label'>Find the weather of your city:</span>
	            </div>
                </div>
                <div className='row'>                
                <div className='form-group col-sm-6'>
                	<input className='form-control' name='field_city' onChange={this.changeCity} defaultValue='' placeholder='Name of the City, Country' style={style} />
                </div>
                <button type='submit' className='btn btn-primary filter_button' disabled={!this.state.city}>Get Weather</button>
                </div>
           </form>
        );
    }
}
