Weather API
===========


Author: Henry Wong
License: MIT
Git: git@bitbucket.org:hwong_earls/gsl_assigment.git
 

##Goals:
- Implement a simply front-end application in Javascript, HTML and CSS.
- Application must communicate with external API via AJAX calls.
- Application implements a simple ReactJS framework.

##Libraries and tools:
- Webpack
- ReactJS
- NodeJS & npm
- Bower
- Bootstrap & JQuery

##Deployment:
- Deployment using Docker container

##Requirements:
- Node and NPM
- Docker

##Make local:
'''
$ npm install 
$ bower install
$ webpack --progress --color
'''

##Launch from container
'''
$./launch
'''

##Launch from local
'''
$./local
'''